
DEFAULT_MAX_NUMBER=2048

THE_MAX_VALUE=${1:-$DEFAULT_MAX_NUMBER}

echo The max value is $THE_MAX_VALUE

THE_NUMBER_IM_THINKING_OF=$((RANDOM%$THE_MAX_VALUE+1))

echo Make a guess:

exit=0
while [ $exit != 1 ]
do
    read input
    if [ $input -lt 1 ]
    then
        echo "You must enter a number that's >= 1"
    else if [ $input -gt $THE_MAX_VALUE ]
    then
        echo "You must enter a number that's <= $THE_MAX_VALUE"
    else if [ $input -gt $THE_NUMBER_IM_THINKING_OF ]
    then
        echo "You must enter a number that's <= $THE_NUMBER_IM_THINKING_OF"

    else if [ $input -lt $THE_NUMBER_IM_THINKING_OF ]
    then
        echo "You must enter a number that's <= $THE_NUMBER_IM_THINKING_OF"
    else if [ $input == $THE_NUMBER_IM_THINKING_OF ]
    then
        echo "You got me."
        echo " /\_/\ "
        echo "( o.o )"
        echo " > ^ < "
    else
        exit=1
    fi
done



